from orthogonalization import *
def adjust(L, multipliers):
    x = L.copy()
    for d in range(len(multipliers)):
        x[d] = multipliers[d]*x[d]
    return x
def orthonormalize(L):
    '''
    Input: a list L of linearly independent Vecs
    Output: A list T of orthonormal Vecs such that for all i in [1, len(L)],
            Span L[:i] == Span T[:i]
    '''
    LL = [ v for v in orthogonalize(L) if v*v > 1E-20]
    #print("blah")
    return [v/((v*v)**0.5) for v in LL]


def aug_orthonormalize(L):
    '''
    Input:
        - L: a list of Vecs
    Output:
        - A pair Qlist, Rlist such that:
            * coldict2mat(L) == coldict2mat(Qlist) * coldict2mat(Rlist)
            * Qlist = orthonormalize(L)
    '''
    vstar, sigma = aug_orthogonalize(L)
    norm_list = list()
    sigma_list = list()
    vstar_nz= list()

    for i in range(len(vstar)):

        norm = vstar[i]*vstar[i]
        vstar_nz.append(vstar[i]/(norm**0.5))
        norm_list.append(norm**0.5)

    sigma_list = [adjust(v,norm_list) for v in sigma]
    return vstar_nz, sigma_list

