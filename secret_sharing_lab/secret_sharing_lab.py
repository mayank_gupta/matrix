# version code 988
# Please fill out this stencil and submit using the provided submission script.

import random
from GF2 import one
from vecutil import list2vec
from vec import Vec
from independence import is_independent
from itertools import combinations



## Problem 1
def randGF2(): return random.randint(0,1)*one

a0 = list2vec([one, one,   0, one,   0, one])
b0 = list2vec([one, one,   0,   0,   0, one])

def choose_secret_vector(s,t):
   u = list2vec([randGF2() for i in range(6)])

   if a0*u == s and b0*u == t:
   	return u
   else:
   	while a0*u !=s or b0*u != t:
   		u = list2vec([randGF2() for i in range(6)])
   	return u

def ran_vec():
	return list2vec([randGF2() for i in range(6)])


def calculate():
	vecs = [(a0,b0)]
	a1 = Vec({0, 1, 2, 3, 4, 5},{0: 0, 1: one, 2: one, 3: one, 4: 0, 5: one})
	a2 = Vec({0, 1, 2, 3, 4, 5},{0: 0, 1: one, 2: 0, 3: one, 4: one, 5: 0})
	b1 = Vec({0, 1, 2, 3, 4, 5},{0: one, 1: 0, 2: 0, 3: 0, 4: one, 5: 0})
	b2 = Vec({0, 1, 2, 3, 4, 5},{0: 0, 1: 0, 2: one, 3: one, 4: 0, 5: 0})
	vecs.append((a1,b1))
	vecs.append((a2,b2))
	for i in range(2):
		a,b = ran_vec(), ran_vec()
		vecs.append((a,b))
		while all(is_independent(list(sum(x, ()))) for x in combinations(vecs,3)) != True:
			vecs.remove((a,b))
			a,b = ran_vec(), ran_vec()
			vecs.append((a,b))
	return vecs

## Problem 2
# Give each vector as a Vec instance
l = calculate()
secret_a0 = l[0][0]
secret_b0 = l[0][1]
secret_a1 = l[1][0]
secret_b1 = l[1][1]
secret_a2 = l[2][0]
secret_b2 = l[2][1]
secret_a3 = l[3][0]
secret_b3 = l[3][1]
secret_a4 = l[4][0]
secret_b4 = l[4][1]
