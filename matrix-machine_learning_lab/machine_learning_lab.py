from mat import *
from vec import *
from cancer_data import *

## Task 1 ##
def signum(u):
    '''
    Input:
        - u: Vec
    Output:
        - v: Vec such that:
            if u[d] >= 0, then v[d] =  1
            if u[d] <  0, then v[d] = -1
    Example:
        >>> signum(Vec({1,2,3},{1:2, 2:-1})) == Vec({1,2,3},{1:1,2:-1,3:1})
        True
    '''
    f = dict()
    for d in u.D:
    	if u[d] >= 0:
    		f[d] = 1
    	else:
    		f[d] = -1
    return Vec(u.D, f)

## Task 2 ##
def fraction_wrong(A, b, w):
    '''
    Input:
        - A: a Mat with rows as feature vectors
        - b: a Vec of actual diagnoses
        - w: hypothesis Vec
    Output:
        - Fraction (as a decimal in [0,1]) of vectors incorrectly
          classified by w 
    '''
    prod = A*w
    ps = signum(prod)
    count = (len(b.D) + ps*b)/2
    return 1- (count/len(b.D))


## Task 3 ##
def loss(A, b, w):
    '''
    Input:
        - A: feature Mat
        - b: diagnoses Vec
        - w: hypothesis Vec
    Output:
        - Value of loss function at w for training data
    '''
    loss_vector = A*w -b
    return loss_vector*loss_vector

## Task 4 ##
def find_grad(A, b, w):
    '''
    Input:
        - A: feature Mat
        - b: diagnoses Vec
        - w: hypothesis Vec
    Output:
        - Value of the gradient function at w
    '''
    diff = A*w - b
    return 2*diff*A

## Task 5 ##
def gradient_descent_step(A, b, w, sigma):
    '''
    Input:
        - A: feature Mat
        - b: diagnoses Vec
        - w: hypothesis Vec
        - sigma: step size
    Output:
        - The vector w' resulting from 1 iteration of gradient descent
          starting from w and moving sigma.
    '''
    gradient = find_grad(A, b, w)
    return w - sigma*gradient


def gradient_descent(A, b, w, sigma, T):
	ww = w.copy()
	for i in range(T):
		if i%30 == 0:
			print('i:',i)
			print('Loss:',loss(A,b,ww))
			print('Fraction wrong:', fraction_wrong(A,b,ww))
		wdash = gradient_descent_step(A, b, ww, sigma)
		ww = wdash

	return ww

